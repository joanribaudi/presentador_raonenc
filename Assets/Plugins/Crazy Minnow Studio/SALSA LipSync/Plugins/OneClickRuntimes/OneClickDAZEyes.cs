﻿using System;
using UnityEngine;
using System.Text.RegularExpressions;

namespace CrazyMinnow.SALSA.OneClicks
{
	public class OneClickDAZEyes : MonoBehaviour
	{
		public static void Setup(GameObject go)
		{
			string emotibody = "^emotiguy.*.shape$";
			string body = "^genesis([238])?(fe)?(male)?.shape$";
			string eyelash = "^genesis([238])?(fe)?(male)?eyelashes.shape$";
			string[] eyelidtopLNames = new string[] {"blink_l", "eyelidstopdownl", "eyelidsupperdownupl", "eyesclosedl"};
			string[] eyelidbotLNames = new string[] {"eyelidsbottomupl", "eyelidslowerupdownl"};
			string[] eyelidtopRNames = new string[] {"blink_r", "eyelidstopdownr", "eyelidsupperdownupr", "eyesclosedr"};
			string[] eyelidbotRNames = new string[] {"eyelidsbottomupr", "eyelidslowerupdownr"};
			string eyelidtopL = String.Empty;
			string eyelidbotL = String.Empty;
			string eyelidtopR = String.Empty;
			string eyelidbotR = String.Empty;
			float blinkMax = 1f;
			int gen = -1; // 0=Emotiguy, 1=Genesis1, 2=Genesis2, 3=Genesis3, 8=Genesis8

			if (go)
			{
				Eyes eyes = go.GetComponent<Eyes>();
				if (eyes == null)
				{
					eyes = go.AddComponent<Eyes>();
				}
				else
				{
					DestroyImmediate(eyes);
					eyes = go.AddComponent<Eyes>();
				}
				QueueProcessor qp = go.GetComponent<QueueProcessor>();
				if (qp == null) qp = go.AddComponent<QueueProcessor>();

				// System Properties
                eyes.characterRoot = go.transform;
                eyes.queueProcessor = qp;

				// Get gen
				Transform smrObj = Eyes.FindTransform(eyes.characterRoot, emotibody);
				SkinnedMeshRenderer smr;
				if (smrObj)
				{
					Debug.Log("smrObj found");
					smr = smrObj.GetComponent<SkinnedMeshRenderer>();
					if (smr)
						if (smr.name.ToLower().Contains("emotiguy")) gen = 0;
				}
				else
				{
					smrObj = Eyes.FindTransform(eyes.characterRoot, body);
					if (smrObj)
					{
						smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
						if (smr)
						{
							if (smr.name.ToLower().Contains("genesis")) gen = 1;
							if (smr.name.ToLower().Contains("genesis2")) gen = 2;
							if (smr.name.ToLower().Contains("genesis3")) gen = 3;
							if (smr.name.ToLower().Contains("genesis8")) gen = 8;
						}
					}
				}

				if (gen > -1)
				{
					// Heads - Bone_Rotation
					eyes.BuildHeadTemplate(Eyes.HeadTemplates.Bone_Rotation_XY);
					eyes.heads[0].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot,"^head$");
					eyes.headTargetOffset.y = 0.058f;
					eyes.FixAllTransformAxes(ref eyes.heads, false);
					eyes.FixAllTransformAxes(ref eyes.heads, true);

					// Eyes - Bone_Rotation
					eyes.BuildEyeTemplate(Eyes.EyeTemplates.Bone_Rotation);
					eyes.eyes[0].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot,"^lEye$");
					eyes.eyes[1].expData.controllerVars[0].bone = Eyes.FindTransform(eyes.characterRoot,"^rEye$");
					eyes.FixAllTransformAxes(ref eyes.eyes, false);
					eyes.FixAllTransformAxes(ref eyes.eyes, true);

					// Eyelids - Bone_Rotation
					eyes.BuildEyelidTemplate(Eyes.EyelidTemplates.BlendShapes); // includes left/right eyelid
					eyes.eyelidPercentEyes = 1f;
					blinkMax = 1f;
					switch (gen)
					{
						case 0: // Emotiguy
							eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Upper);
							eyes.headTargetOffset.y = 0.85f;
							eyes.headRandDistRange = new Vector2(3f, 3f);
							eyes.eyeRandDistRange = new Vector2(3f, 3f);
							eyes.eyelidTracking = false;
							eyelidtopL = eyelidtopLNames[0];
							eyelidtopR = eyelidtopRNames[0];
							// Left upper eyelid
							eyes.eyelids[0].referenceIdx = 0;
							eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, emotibody).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
							// Right upper eyelid
							eyes.eyelids[1].referenceIdx = 1;
							eyes.eyelids[1].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, emotibody).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
							break;
						case 1: // Genesis 1
							eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Upper);
							eyelidtopL = eyelidtopLNames[3];
							eyelidtopR = eyelidtopRNames[3];
							// Left upper eyelid
							eyes.eyelids[0].referenceIdx = 0;
							eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
							// Right upper eyelid
							eyes.eyelids[1].referenceIdx = 1;
							eyes.eyelids[1].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
							break;
						case 2: // Genesis 2
							eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Both);
							eyelidtopL = eyelidtopLNames[1];
							eyelidbotL = eyelidbotLNames[0];
							eyelidtopR = eyelidtopRNames[1];
							eyelidbotR = eyelidbotRNames[0];
							// Left upper eyelid
							eyes.eyelids[0].referenceIdx = 0;
							eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
							// Left lower eyelid
							eyes.eyelids[0].expData.controllerVars[1].smr = eyes.eyelids[0].expData.controllerVars[0].smr;
							eyes.eyelids[0].expData.controllerVars[1].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[1].smr, eyelidbotL);
							eyes.eyelids[0].expData.controllerVars[1].maxShape = blinkMax;
							// Right upper eyelid
							eyes.eyelids[1].referenceIdx = 1;
							eyes.eyelids[1].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
							// Right lower eyelid
							eyes.eyelids[1].expData.controllerVars[1].smr = eyes.eyelids[1].expData.controllerVars[0].smr;
							eyes.eyelids[1].expData.controllerVars[1].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[1].smr, eyelidbotR);
							eyes.eyelids[1].expData.controllerVars[1].maxShape = blinkMax;
							break;
						case 3: // Genesis 3
							eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Both);
							eyelidtopL = eyelidtopLNames[2];
							eyelidbotL = eyelidbotLNames[1];
							eyelidtopR = eyelidtopRNames[2];
							eyelidbotR = eyelidbotRNames[1];
							// Left upper eyelid
							eyes.eyelids[0].referenceIdx = 0;
							eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
							// Left lower eyelid
							eyes.eyelids[0].expData.controllerVars[1].smr = eyes.eyelids[0].expData.controllerVars[0].smr;
							eyes.eyelids[0].expData.controllerVars[1].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[1].smr, eyelidbotL);
							eyes.eyelids[0].expData.controllerVars[1].maxShape = blinkMax;
							// Right upper eyelid
							eyes.eyelids[1].referenceIdx = 1;
							eyes.eyelids[1].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
							// Right lower eyelid
							eyes.eyelids[1].expData.controllerVars[1].smr = eyes.eyelids[0].expData.controllerVars[0].smr;
							eyes.eyelids[1].expData.controllerVars[1].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[1].smr, eyelidbotR);
							eyes.eyelids[1].expData.controllerVars[1].maxShape = blinkMax;
							break;
						case 8: // Genesis 8
							eyes.SetEyelidShapeSelection(Eyes.EyelidSelection.Upper);
							eyes.AddEyelidShapeExpression();
							eyes.AddEyelidShapeExpression();
							eyelidtopL = eyelidtopLNames[3];
							eyelidtopR = eyelidtopRNames[3];
							// Left upper eyelid
							eyes.eyelids[0].referenceIdx = 0;
							eyes.eyelids[0].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[0].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[0].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[0].expData.controllerVars[0].maxShape = blinkMax;
							// Right upper eyelid
							eyes.eyelids[1].referenceIdx = 1;
							eyes.eyelids[1].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, body).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[1].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[1].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[1].expData.controllerVars[0].maxShape = blinkMax;
							// Left upper eyelash
							eyes.eyelids[2].referenceIdx = 0;
							eyes.eyelids[2].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, eyelash).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[2].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[2].expData.controllerVars[0].smr, eyelidtopL);
							eyes.eyelids[2].expData.controllerVars[0].maxShape = blinkMax;
							// Right upper eyelash
							eyes.eyelids[3].referenceIdx = 1;
							eyes.eyelids[3].expData.controllerVars[0].smr = Eyes.FindTransform(eyes.characterRoot, eyelash).GetComponent<SkinnedMeshRenderer>();
							eyes.eyelids[3].expData.controllerVars[0].blendIndex = Eyes.FindBlendIndex(eyes.eyelids[3].expData.controllerVars[0].smr, eyelidtopR);
							eyes.eyelids[3].expData.controllerVars[0].maxShape = blinkMax;
							break;
					}
					// Initialize the Eyes module
					eyes.Initialize();
				}
			}
		}
	}
}