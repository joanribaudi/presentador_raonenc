﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlAudio : MonoBehaviour
{
    public string audioName;
    public string imageName;


    [Header("Audio Stuff")]
    public AudioSource audioSource;
    public AudioClip audioClip;
    public RawImage imageBack;
    
    public string soundPath, imagePath;

    public Texture2D LoadPNG(string filename)
    {

        Texture2D tex = null;
        byte[] fileData;

        // string imageToLoad = "/Assets/StreamingAssets/image/" + filename;

        string imageToLoad = Application.streamingAssetsPath + "/image/"+ filename;

        print(imageToLoad);

       // if (System.IO.File.Exists(filename))
        //{
            fileData = System.IO.File.ReadAllBytes(imageToLoad);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        //}

       return tex;
    }


    private void Awake()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        soundPath = "file://" + Application.streamingAssetsPath + "/sound/";
        imagePath = "file://" + Application.streamingAssetsPath + "/image/";

        print(imagePath);
        print(soundPath);
        //StartCoroutine(LoadAudio());
    }

      public IEnumerator LoadAudio()
    {

        print(imageName);

        imageBack.texture = LoadPNG(imageName);


        WWW request = GetAudioFromFile(soundPath, audioName);
        yield return request;

        audioClip = request.GetAudioClip();
        audioClip.name = audioName;

        PlayAudioFile();


        //string imageToLoad = string.Format(imagePath + "{0}", imageName);

       
       


    }

    private void PlayAudioFile()
    {
        audioSource.clip = audioClip;
        audioSource.Play();
        audioSource.loop = true;
    }

    private WWW GetAudioFromFile(string path, string filename)
    {
        string audioToLoad = string.Format(path + "{0}", filename);
        WWW request = new WWW(audioToLoad);
        return request;
    }

    public void LocutarNoticia(string nomFitxerNoticia)
    {
        audioName = nomFitxerNoticia+".wav";
        imageName = nomFitxerNoticia + ".png";
        StartCoroutine(LoadAudio());

    }





}
